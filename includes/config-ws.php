<?php





/* * * * COMMON * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
define ("BROWSED_LINKS_TO_FILE",       dirname(__FILE__).'/../'."ws-browsed-".date("Ymd-His").".txt");
define ("COOKIES",                     dirname(__FILE__).'/../'."ws-cookies-".date("Ymd-His").".txt"); //Browser Cookies location
define ("BROWSER",                     "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36"); //browser definition
define ("LOG_FILE",                    dirname(__FILE__).'/../'."ws-log-".date("Ymd-His").".txt");
define ("ENCHUFIX_SERVER",             'ftp.enchufix.com');
define ("ENCHUFIX_FTP_USER",           'ecodomia@ecodomia.com');
define ("ENCHUFIX_FTP_PASS",           'EnchF1240');

define ("RESIZED_IMAGE_MAX_WIDTH",     800);
define ("RESIZED_IMAGE_MAX_HEIGHT",    800);





/* * * * MIMAX * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

define ("MIMAX_URL",                    'http://www.searchlightelectric.com/');
define ("MIMAX_CSV",                    dirname(__FILE__).'/../'."web_data_mimax.csv");
define ("MIMAX_DOWNLOAD_IMAGES",        true);
define ("MIMAX_LOC_IMG_ORG1",           dirname(__FILE__).'/../'."images-mmx-1/");
//define ("MIMAX_LOC_IMG_ORG1",         'D:\FOTOS\MIMAX-TMP\0-descargadas');
define ("MIMAX_LOC_IMG_ORG2",           dirname(__FILE__).'/../'."images-mmx-2/");
//define ("MIMAX_LOC_IMG_ORG2",         'D:\FOTOS\MIMAX-TMP\0-originales');
define ("MIMAX_IMG_PRFX",               'mimax-');
define ("EXCH_RATE_GPB_EUR",            1.30652114);




/* * * * GRUPO NOVOLUX * * * * * * * * * * * * * * * * * * * * * * * * * * * */

define ("GNX1_USER",                    'admin@enchufix.com');
define ("GNX1_PASS",                    '2605AEea');

define ("GNX1_WITH_PRICES",             false);
define ("GNX1_PRICE_MARGIN",            '0.25');
define ("GNX1_PVR_DISCOUNT",            '0.45');
define ("GNX1_DAYS_SHIPPING",           '3');

//define ("GNX1_CSV_IMPORT_LOCAL",      dirname(__FILE__).'/../'."import_products_gnovolux_stock.csv");
define ("GNX1_CSV_IMPORT_LOCAL",        'D:/USUARIOS/Oscar/EMPRESA/HOST/public_html/www.enchufix.com/tienda/var/import'.'/'."import_products_gnovolux_stock.csv");
define ("GNX1_CSV_IMPORT_REMOTE",       '/public_html/www.enchufix.com/tienda/var/import'.'/'."import_products_gnovolux_stock.csv");
//define ("GNX1_CSV_DATA",              dirname(__FILE__).'/../'."web_data_gnovolux_stock.csv");
define ("GNX1_CSV_DATA",                'D:/USUARIOS/Oscar/EMPRESA/OZERTIS/MERCADO/PROVEEDORES y MARCAS/Grupo Novolux/DATOS'.'/'."web_data_gnovolux.csv");

$filterarr = array(
    array(
        "cat"=>'SUMERGIBLES',
        "days"=>'14',
        "stock-status"=>'B. pedido (15d)'
    ),
    array(
        "cat"=>'CL&Aacute;SICO RESIDENCIAL',
        "days"=>'7',
        "stock-status"=>'B. pedido (7d)'
    )
);




/* * * * GNX * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
define ("GNX2_URL1",                    'http://out.cristher.com/wcorp/es/');
define ("GNX2_URL2",                    'http://out.exolighting.com/wcorp/es/');
define ("GNX2_URL3",                    'http://out.dopo.es/wcorp/es/');
define ("GNX2_LINK_FILE",               dirname(__FILE__).'/../'."web_data_gnx_productlinks.txt");
define ("GNX2_CSV_DATA",                dirname(__FILE__).'/../'."web_data_gnx.csv");
define ("GNX2_DOWNLOAD_IMAGES",         true);
define ("GNX2_IMG_LOC",                 dirname(__FILE__).'/../'."images-gnx-1/");
define ("GNX2_IMG_PRFX",                'gnx-');






/* * * * UNIX * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

define ("UNIX_USER",                   'info@enchufix.com');
define ("UNIX_PASS",                   'EnchF1240');

define ("UNIX_PRICE_THRESHOLD",        '7');

define ("UNIX_LOC_XML_IX",              dirname(__FILE__).'/../'."IXIA.xml");
define ("UNIX_LOC_XML_JU",              dirname(__FILE__).'/../'."JUINSA.xml");
define ("UNIX_LOC_XML_UN",              dirname(__FILE__).'/../'."UNIMASA.xml");
// define ("UNIX_LOC_XML_IX",              'D:/USUARIOS/Oscar/EMPRESA/OZERTIS/MERCADO/PROVEEDORES y MARCAS/Ixia & Unimasa/DATOS'.'/'."IXIA.xml");
// define ("UNIX_LOC_XML_JU",              'D:/USUARIOS/Oscar/EMPRESA/OZERTIS/MERCADO/PROVEEDORES y MARCAS/Ixia & Unimasa/DATOS'.'/'."JUINSA.xml");
// define ("UNIX_LOC_XML_UN",              'D:/USUARIOS/Oscar/EMPRESA/OZERTIS/MERCADO/PROVEEDORES y MARCAS/Ixia & Unimasa/DATOS'.'/'."UNIMASA.xml");

define ("UNIX_TEST_CATEGORY",                   '/');

define ("UNIX_DOWNLOAD_IMAGES",         true);
define ("UNIX_RESIZE_IMAGES",           true);
define ("UNIX_LOC_IMG_ORG1",            dirname(__FILE__).'/../'."images-unix-1/");
//define ("UNIX_LOC_IMG_ORG1",          'D:\FOTOS\UNIX-TMP\0-descargadas');
define ("UNIX_LOC_IMG_ORG2",            dirname(__FILE__).'/../'."images-unix-2/");
//define ("UNIX_LOC_IMG_ORG2",          'D:\FOTOS\UNIX-TMP\0-originales');
define ("UNIX_IMG_PRFX",                'unix-');





/* * * * FERMAX * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
define ("FERMAX_URL",                    'http://www.fermax.com/spain/pro/productos/');
define ("FERMAX_CSV",                    dirname(__FILE__).'/../'."/web_data_fermax.csv");




?>