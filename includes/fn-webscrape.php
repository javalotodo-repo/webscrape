<?php


require_once(dirname(__FILE__) . '/../lib/goutte-v2.0.4.phar');
use Goutte\Client;












/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/* COMMON
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**
 * Logging class:
 * - contains lfile, lwrite and lclose public methods
 * - lfile sets path and name of log file
 * - lwrite writes message to the log file (and implicitly opens log file)
 * - lclose closes log file
 * - first call of lwrite method will open log file implicitly
 * - message is written with the following format: [d/M/Y:H:i:s] (script name) message
 */
class Logging {
    // declare log file and file pointer as private properties
    private $log_file, $fp;
    // set log file (path and name)
    public function lfile($path) {
        $this->log_file = $path;
    }
    // write message to the log file
    public function lwrite($message) {
        // if file pointer doesn't exist, then open log file
        if (!is_resource($this->fp)) {
            $this->lopen();
        }
        // define script name
        $condcript_name = pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME);
        // define current time and suppress E_WARNING if using the system TZ settings
        // (don't forget to set the INI setting date.timezone)
        $time = @date('d/M/Y H:i:s');
        // write current time, script name and message to the log file
        fwrite($this->fp, "$time\t$message" . PHP_EOL);
    }
    // close log file (it's always a good idea to close a file when you're done with it)
    public function lclose() {
        fclose($this->fp);
    }
    // open log file (private method)
    private function lopen() {
        // in case of Windows set default log file
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $log_file_default = 'c:/php/logfile.txt';
        }
        // set default log file for Linux and other systems
        else {
            $log_file_default = '/tmp/logfile.txt';
        }
        // define log file from lfile method or use previously set default
        $lfile = $this->log_file ? $this->log_file : $log_file_default;
        // open log file for writing only and place file pointer at the end of the file
        // (if the file does not exist, try to create it)
        $this->fp = fopen($lfile, 'a') or exit("Can't open $lfile!");
    }
    public function lecho($message, $lineBreak = true) {
        $time = @date('d/M/Y H:i:s');
        echo "$time\t$message".(($lineBreak)?PHP_EOL:"");
    }
    public function lwecho($message) {
        self::lecho($message);
        self::lwrite($message);
    }
}

function ftpUploadFile($ftpserver, $ftpuser, $ftppass, $locFile, $remFile, $silent = false, $debug = false) {

    $log = new Logging();
    $time_start = microtime(true);
    if (!$silent) $log->lecho (PHP_EOL."-------------------------- FTP UPLOAD FILE -------");
    if (!$silent) $log->lecho("Subiendo archivo local '".$locFile."' al servidor ".$ftpserver.", archivo remoto '".$remFile."'");

    // connect to FTP server (port 21)
    $conn = ftp_connect($ftpserver, 21)             or die ("No se puede conectar a ".$ftpserver);
    $login = ftp_login($conn, $ftpuser, $ftppass)     or die("Error en la autentificacion (".$ftpuser." / ".$ftppass.")");                // send access parameters

    ftp_pasv ($conn, true);                                                // turn on passive mode transfers (some servers need this)
     ftp_chdir($conn, dirname($remFile));
    $upload = ftp_put($conn, $remFile, $locFile, FTP_BINARY);        // perform file upload
    ftp_close($conn);
    if (!$upload) die('ERROR al subir el archivo '.$locFile);

    $time = microtime(true) - $time_start;
    if (!$silent) $log->lecho ("Completado en ".number_format($time,2)."s.");

    return true;
}

// Saves an array of values in a given CSV
// Note: structure of array must correspond with structure of CSV)
function saveArrayInCsv($csvfile, $array) {
    $fp = fopen($csvfile, 'a') or die("can't open csv file");
    //fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
    $list[] = $array;
    foreach ($list as $fields) {
        fputcsv($fp, $fields);
    }
    fclose($fp);
}

// Downloads any file (image, PDF... etc.) 
function downloadFile($fileurl, $filepath) {
	
    if (!file_exists($filepath)) {
        
        ob_clean();
        $fp = fopen($filepath, 'w+');                 // open file handle
        ob_end_flush();

        $ch = curl_init($fileurl);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // enable if you want
        curl_setopt($ch, CURLOPT_FILE, $fp);          // output to file
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);        // some large value to allow curl to run for a long time
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36');
        // curl_setopt($ch, CURLOPT_VERBOSE, true);   // enable this line to see debug prints
        curl_exec($ch);

        curl_close($ch);                              // closing curl handle
        fclose($fp);
    }
}

// Records browsed URL / product so is not scraped twice
function saveBrowsedUrl($val, $filename) {
    if (file_exists($filename)) {
        $file = file_get_contents($filename);
    } else {
        file_put_contents($filename, '');
    }
    $file = file_get_contents($filename);

    if (!stristr($file, $val)) {
        $fh = fopen($filename, 'a') or die("can't open file");
        fwrite($fh, $val . PHP_EOL);
        fclose($fh);
    }
}

// Checks if URL has been already browsed
function urlNotBrowsed($url) {
    if (!file_exists(BROWSED_LINKS_TO_FILE))
    {
        $fh = fopen(BROWSED_LINKS_TO_FILE, 'w') or die("can't open file");
        fclose($fh);
    }
    $content = @file_get_contents(BROWSED_LINKS_TO_FILE);

    $arr = explode(PHP_EOL,$content);
    if (in_array($url,$arr))
        return false;
    else return true;
}

// Goes to URL and captures HTML content
function curlDownload($ch, $url) {
    if (!function_exists('curl_init')) {
        die('Data Stream Curently Unavailable');
    }
    //$ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_REFERER,$url);
    curl_setopt($ch, CURLOPT_USERAGENT, BROWSER);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_COOKIEJAR, COOKIES);
    curl_setopt($ch, CURLOPT_COOKIEFILE, COOKIES);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept-Language: en-US,en;q=0.8'));
    $output = curl_exec($ch);
    //curl_close($ch);
    //sleep(rand(3, 5));
    return $output;
}

// Optimize & resize one (product) image (GIF, JPEG or PNG)
function resizeOptimizeImage ($src_path, $dst_path, $new_max_width = 1000, $new_max_height = 1000, $quality = 75) {
    list($src_width, $src_height, $src_type) = getimagesize($src_path);
    switch ($src_type) {
        case IMAGETYPE_GIF:
            $source_gd_image = imagecreatefromgif($src_path);
            break;
        case IMAGETYPE_JPEG:
            $source_gd_image = imagecreatefromjpeg($src_path);
            break;
        case IMAGETYPE_PNG:
            $source_gd_image = imagecreatefrompng($src_path);
            break;
    }
    if ($source_gd_image === false) {
        return false;
    }
    if ($src_width==0||$src_height==0) return false;
    $source_aspect_ratio = $src_width / $src_height;
    $thumbnail_aspect_ratio = $new_max_width / $new_max_height;
    if ($src_width <= $new_max_width && $src_height <= $new_max_height) {
        $thumbnail_image_width = $src_width;
        $thumbnail_image_height = $src_height;
    } elseif ($thumbnail_aspect_ratio > $source_aspect_ratio) {
        $thumbnail_image_width = (int) ($new_max_height * $source_aspect_ratio);
        $thumbnail_image_height = $new_max_height;
    } else {
        $thumbnail_image_width = $new_max_width;
        $thumbnail_image_height = (int) ($new_max_width / $source_aspect_ratio);
    }
    $thumbnail_gd_image = imagecreatetruecolor($thumbnail_image_width, $thumbnail_image_height);
    imagecopyresampled($thumbnail_gd_image, $source_gd_image, 0, 0, 0, 0, $thumbnail_image_width, $thumbnail_image_height, $src_width, $src_height);
    imagejpeg($thumbnail_gd_image, $dst_path, $quality);
    imagedestroy($source_gd_image);
    imagedestroy($thumbnail_gd_image);
    return true;
}

// Simple string functions
function strStartsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
}
function strEndsWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
}

function isInCsv($pid,$csvfile) {
    $file = fopen($csvfile, 'r');
    while (($line = fgetcsv($file)) !== FALSE) {
        foreach($line as $val) {
            if(stristr($val,$pid)) return true;
        }
    }
    fclose($file);
    return false;
}









/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/* GRUPO NOVOLUX (PRECIOS/STOCK)
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
function gnx1NewWebDataCsv($csvfile) {
    $log = new Logging();
    if (file_exists($csvfile)) {
        unlink($csvfile);
    }
    if (file_exists($csvfile)) {
        $log->lecho ("Existing file $csvfile could NOT be deleted");
        exit();
    }
    if (!file_exists($csvfile)) {
        $list = array();
        $fp = fopen($csvfile, 'w+');
        //fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
        $list[] = array('category','id','name','availavility','is_in_stock','envio','price');
        foreach ($list as $fields) {
            fputcsv($fp,$fields);
        }
        fclose($fp);
    }
}
function gnx1NewImportCsv($csvfile) {
    $log = new Logging();
    if (file_exists($csvfile)) {
        unlink($csvfile);
    }
    if (file_exists($csvfile)) {
        $log->lecho ("Existing file $csvfile could NOT be deleted");
        exit();
    }
    if (!file_exists($csvfile)) {
        $list = array();
        $fp = fopen($csvfile, 'w+');
        //fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
        if (GNX1_WITH_PRICES)
            $list[] = array('websites','sku','status','qty','is_in_stock','custom_stock_status','envio','badge','cost','price','special_price');
        else
            $list[] = array('websites','sku','status','qty','is_in_stock','custom_stock_status','envio','badge');
        foreach ($list as $fields) {
            fputcsv($fp,$fields);
        }
        fclose($fp);
    }
}
function gnx1NextPageNumber($content) {
    $pattern = '/id=[\"\']pagination_next[\"\'].*href.*p=(.*)[\"\']/Usi';
    preg_match($pattern, $content, $result);
    $nextpage=trim($result[1]);
    if(is_numeric($nextpage))
        return $nextpage;
    else
        return false;
}
function gnx1Sku($content) {
    $pattern = '/<h3.*>(.*)<\/h3>/Usi';
    preg_match($pattern, $content, $result);
    return @trim(preg_replace('/\s\s+/Usi', '', preg_replace('/<.*>/Usi', '', $result[1])));
}
function gnx1Name($content) {
    $pattern = '/<a\s*class=[\"\']title\-producto[\"\'].*title=[\"\'](.*)[\"\']/Usi';
    preg_match($pattern, $content, $result);
    $name= @trim(preg_replace('/\s\s+/Usi', '', preg_replace('/<.*>/Usi', '', $result[1])));
    $name=html_entity_decode($name, ENT_QUOTES, 'UTF-8');
    return $name=mb_convert_encoding($name, 'UTF-16LE', 'UTF-8');
}
function gnx1Price($content) {
    $pattern = '/<span\s*class=[\"\']price[\"\'].*>(.*)</Usi';
    preg_match($pattern, $content, $result);
    $price=@trim(preg_replace('/\s\s+/Usi', '', preg_replace('/<.*>/Usi', '', $result[1])));
    return $price=preg_replace('/[^0-9\,]/Usi','',$price);
}
function gnx1Status($content) {
    $pattern = '/<span\s*class=[\"\']availability[\"\'].*>(.*)<\/span>/Usi';
    preg_match($pattern, $content, $result);
    $data=$result[1];
    if (stristr($data,'no-stock.gif'))
        return '0';
    elseif (stristr($data,'si-stock.gif'))
        return '1';
}










/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/* GRUPO NOVOLUX (PRODUCTOS)
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
function gnx2StartCrawling($url, $debug = false) {
    gnx2GetMenuLinks($url, $debug);
    if(!is_dir(GNX2_IMG_LOC))
        mkdir(GNX2_IMG_LOC,0777);
    gnx2ScrapeProducts($debug);
}

function gnx2GetMenuLinks($url, $debug = false) {
    $contents = gnx2FetchContents($url);
    $pattern = '/<div\s*id=[\"\']block_top_menu[\"\'].*<\/div>(.*)<\/div>/Usi';
    preg_match($pattern, $contents, $result);

    $cont = $result[1];

    $pattern = '/<ul.*<ul>(.*)<\/ul>/Usi';
    preg_match_all($pattern, $cont, $result);

    // Get all Series URLs
    $i=0;
    foreach($result[1] as $portion) {
        if ($debug&&$i>1) break; $i++;
        $pattern = '/<a.*href=[\"\'](.*)[\"\']/Usi';
        preg_match_all($pattern, $portion, $res);
        //print_r($res[1]);  exit;

        // Get all Products URLs
        $j=0;
        foreach($res[1] as $serieurl) {
            if ($debug&&$j>1) break; $j++;
            gnx2GetProductUrls($serieurl, $debug);
        }
    }
}

//Get products URLs of a Serie
function gnx2GetProductUrls($serieurl, $debug = false) {
    $contents = gnx2FetchContents($serieurl);
    $pattern = '/<a\s*class=[\"\']product\-name[\"\']\s*href=[\"\'](.*)[\"\']/Usi';
    preg_match_all($pattern, $contents, $result);
    $i=0;
    foreach($result[1] as $produrl) {
        if ($debug&&$i>1) break; $i++;
        echo $produrl.'<br>\n'.PHP_EOL;
        saveBrowsedUrl($produrl,GNX2_LINK_FILE);
    }
}

function gnx2ScrapeProducts($debug = false) {
    $content = file_get_contents(GNX2_LINK_FILE);
    $arr = explode(PHP_EOL,$content);
    foreach($arr as $link) {
        if(!isInCsv($link,GNX2_CSV_DATA))
            gnx2ScrapeUrlData($link, $debug);
    }
}

function gnx2ScrapeUrlData($url, $debug) {
    $contents = gnx2FetchContents($url);
    @$prodref=gnx2Prodref($contents);
    @$prodname=gnx2Prodname($contents);
    @$lampHolder=gnx2LampHolder($contents);
    @$IP=gnx2IP($contents);
    @$Feeding=gnx2Feeding($contents);
    @$Material=gnx2Material($contents);
    @$Family=gnx2Family($contents);
    @$IK=gnx2IK($contents);
    @$Energyrating=gnx2Energyrating($contents);
    @$Class=gnx2Class($contents);
    @$Completewithlamp=gnx2Completewithlamp($contents);
    @$Finish=gnx2Finish($contents);
    @$Lamp=gnx2Lamp($contents);
    @$breadcumb=gnx2breadcumb($contents);
    @$fotoproducto=gnx2fotoproducto($contents);
    @$productoutline=gnx2productoutline($contents);
    @$Datasheet=gnx2Datasheet($contents);
    @$Fichetechnique=gnx2Fichetechnique($contents);
    //'Url','Product Reference','Product Name','Breadcrumb',mb_convert_encoding('Portalámparas', 'UTF-16LE', 'UTF-8'),'IP',mb_convert_encoding('Alimentación', 'UTF-16LE', 'UTF-8'),'Material','Familia','IK',mb_convert_encoding('Clase Energética', 'UTF-16LE', 'UTF-8'),'Clase',mb_convert_encoding('Lámpara Incluida', 'UTF-16LE', 'UTF-8'),'Acabado',mb_convert_encoding('Lámpara', 'UTF-16LE', 'UTF-8'),'Foto producto','Esquema producto',mb_convert_encoding('Ficha técnica (ESP)', 'UTF-16LE', 'UTF-8'),'Fiche technique (FRA)'
    $array=array(
        'url'=>$url,
        'prodref'=>$prodref,
        'prodname'=>$prodname,
        'breadcumb'=>$breadcumb,
        'lampHolder'=>$lampHolder,
        'IP'=>$IP,
        'Feeding'=>$Feeding,
        'Material'=>$Material,
        'Family'=>$Family,
        'IK'=>$IK,
        'Energyrating'=>$Energyrating,
        'Class'=>$Class,
        'Completewithlamp'=>$Completewithlamp,
        'Finish'=>$Finish,
        'Lamp'=>$Lamp,
        'fotoproducto'=>$fotoproducto,
        'productoutline'=>$productoutline,
        'Datasheet'=>$Datasheet,
        'Fichetechnique'=>$Fichetechnique,
    );

    if ($debug) print_r($array);
    
    if($prodref!='')
        saveArrayInCsv(GNX2_CSV_DATA,$array);

    saveBrowsedUrl($url, BROWSED_LINKS_TO_FILE);
}

function gnx2GrabImage($fileurl, $filepath){

    if (!file_exists($filepath)) {
		
        ob_clean();
        $fp = fopen($filepath,'w+');
        ob_end_flush();

        $ch = curl_init ($fileurl);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        $raw=curl_exec($ch);
        curl_close ($ch);
        // if(file_exists($filepath)){
            // unlink($filepath);
        // }
        // $fp = fopen($filepath,'x');
        fwrite($fp, $raw);
        
        fclose($fp);
    }
}

function gnx2Fichetechnique($contents) {
    $pattern = '/>\s*Fiche\s*technique\s*\(FRA\).*<td>\s*<a.*href=[\'\"](.*)[\'\"]/Usi';
    preg_match($pattern, $contents, $result);
    $name=html_entity_decode($result[1], ENT_QUOTES, 'UTF-8');
    $name=mb_convert_encoding($name, 'UTF-16LE', 'UTF-8');
    $arr=explode('=',$name);
    $imgname=end($arr).'.pdf';
    gnx2GrabImage(GNX2_IMG_LOC.$imgname, file_get_contents($name));
    echo file_get_contents($name);
    return $imgname;
}
function gnx2Datasheet($contents) {
    $pattern = '/>\s*Ficha\s*t.+cnica\s*\(ESP\).*<td>\s*<a.*href=[\'\"](.*)[\'\"]/Usi';
    preg_match($pattern, $contents, $result);
    $name=html_entity_decode($result[1], ENT_QUOTES, 'UTF-8');
    $name=mb_convert_encoding($name, 'UTF-16LE', 'UTF-8');
    $arr=explode('=',$name);
    $imgname=end($arr).'.pdf';
    gnx2GrabImage(GNX2_IMG_LOC.$imgname,file_get_contents($name));
    echo file_get_contents($name);
    return $imgname;
}
function gnx2productoutline($contents) {
    $pattern = '/>\s*Esquema\s*producto.*<td>\s*<a.*href=[\'\"](.*)[\'\"]/Usi';
    preg_match($pattern, $contents, $result);
    $name=html_entity_decode($result[1], ENT_QUOTES, 'UTF-8');
    $name=mb_convert_encoding($name, 'UTF-16LE', 'UTF-8');
    $arr=explode('=',$name);
    $imgname=end($arr).'.jpg';
    gnx2GrabImage(GNX2_IMG_LOC.$imgname,file_get_contents($name));
    echo file_get_contents($name);
    return $imgname;
}
function gnx2fotoproducto($contents) {
    $pattern = '/>\s*Foto\s*producto.*<td>\s*<a.*href=[\'\"](.*)[\'\"]/Usi';
    preg_match($pattern, $contents, $result);
    $name=html_entity_decode($result[1], ENT_QUOTES, 'UTF-8');
    $name=mb_convert_encoding($name, 'UTF-16LE', 'UTF-8');
    $arr=explode('=',$name);
    $imgname=end($arr).'.jpg';
    gnx2GrabImage(GNX2_IMG_LOC.$imgname,file_get_contents($name));
    echo file_get_contents($name);
    return $imgname;
}

function gnx2breadcumb($contents) {
    $pattern = '/<div\s*class=[\"\']breadcrumb\s*clearfix[\"\'].*>(.*)<\/div>/Usi';
    preg_match($pattern, $contents, $result);

    $cont = preg_replace('/<.*>/Usi','',$result[1]);
    $cont = preg_replace('/\s\s+/Usi','',$cont);
    $name=html_entity_decode($cont, ENT_QUOTES, 'UTF-8');
    return $name=mb_convert_encoding($name, 'UTF-16LE', 'UTF-8');
}
function gnx2Lamp($contents) {
    $pattern = '/L.+mpara\&nbsp\;\s*<\/label>.*<span>(.*)<\/span>/Usi';
    preg_match($pattern, $contents, $result);
    $name=html_entity_decode($result[1], ENT_QUOTES, 'UTF-8');
    return $name=mb_convert_encoding($name, 'UTF-16LE', 'UTF-8');
}
function gnx2Finish($contents) {
    $pattern = '/<td>\s*Acabado\s*<\/td>\s*<td.*>(.*)<\/td>/Usi';
    preg_match($pattern, $contents, $result);
    $name=html_entity_decode($result[1], ENT_QUOTES, 'UTF-8');
    return $name=mb_convert_encoding($name, 'UTF-16LE', 'UTF-8');
}
function gnx2Completewithlamp($contents) {
    $pattern = '/<td>\s*L.+mpara\s*Incluida\s*<\/td>\s*<td.*>(.*)<\/td>/Usi';
    preg_match($pattern, $contents, $result);
    $name=html_entity_decode($result[1], ENT_QUOTES, 'UTF-8');
    return $name=mb_convert_encoding($name, 'UTF-16LE', 'UTF-8');
}
function gnx2Class($contents) {
    $pattern = '/<td>\s*Clase\s*<\/td>\s*<td.*>(.*)<\/td>/Usi';
    preg_match($pattern, $contents, $result);
    $name=html_entity_decode($result[1], ENT_QUOTES, 'UTF-8');
    return $name=mb_convert_encoding($name, 'UTF-16LE', 'UTF-8');
}
function gnx2Energyrating($contents) {
    $pattern = '/<td>\s*Clase\s*Energ.+tica\s*<\/td>\s*<td.*>(.*)<\/td>/Usi';
    preg_match($pattern, $contents, $result);
    $name=html_entity_decode($result[1], ENT_QUOTES, 'UTF-8');
    return $name=mb_convert_encoding($name, 'UTF-16LE', 'UTF-8');
}
function gnx2IK($contents) {
    $pattern = '/<td>\s*IK\s*<\/td>\s*<td.*>(.*)<\/td>/Usi';
    preg_match($pattern, $contents, $result);
    $name=html_entity_decode($result[1], ENT_QUOTES, 'UTF-8');
    return $name=mb_convert_encoding($name, 'UTF-16LE', 'UTF-8');
}
function gnx2Family($contents) {
    $pattern = '/<td>\s*Familia\s*<\/td>\s*<td.*>(.*)<\/td>/Usi';
    preg_match($pattern, $contents, $result);
    $name=html_entity_decode($result[1], ENT_QUOTES, 'UTF-8');
    return $name=mb_convert_encoding($name, 'UTF-16LE', 'UTF-8');
}
function gnx2Material($contents) {
    $pattern = '/<td>\s*Material\s*<\/td>\s*<td.*>(.*)<\/td>/Usi';
    preg_match($pattern, $contents, $result);
    $name=html_entity_decode($result[1], ENT_QUOTES, 'UTF-8');
    return $name=mb_convert_encoding($name, 'UTF-16LE', 'UTF-8');
}
function gnx2Feeding($contents) {
    $pattern = '/<td>\s*Alimentaci.+n\s*<\/td>\s*<td.*>(.*)<\/td>/Usi';
    preg_match($pattern, $contents, $result);
    $name=html_entity_decode($result[1], ENT_QUOTES, 'UTF-8');
    return $name=mb_convert_encoding($name, 'UTF-16LE', 'UTF-8');
}
function gnx2IP($contents) {
    $pattern = '/<td>\s*IP\s*<\/td>\s*<td.*>(.*)<\/td>/Usi';
    preg_match($pattern, $contents, $result);
    $name=html_entity_decode($result[1], ENT_QUOTES, 'UTF-8');
    return $name=mb_convert_encoding($name, 'UTF-16LE', 'UTF-8');
}
function gnx2lampHolder($contents) {
    $pattern = '/<td>\s*Portal.+mparas\s*<\/td>\s*<td.*>(.*)<\/td>/Usi';
    preg_match($pattern, $contents, $result);
    $name=html_entity_decode($result[1], ENT_QUOTES, 'UTF-8');
    return $name=mb_convert_encoding($name, 'UTF-16LE', 'UTF-8');
}
function gnx2prodname($contents) {
    $pattern = '/<p\s*class=[\"\']product_name[\"\'].*<p>(.*)<\/p>/Usi';
    preg_match($pattern, $contents, $result);
    $name=html_entity_decode($result[1], ENT_QUOTES, 'UTF-8');
    return $name=mb_convert_encoding($name, 'UTF-16LE', 'UTF-8');
}
function gnx2prodref($contents) {
    $pattern = '/var\s*productReference\s*=\s*[\'\"](.*)[\'\"]/Usi';
    preg_match($pattern, $contents, $result);
    $name=html_entity_decode($result[1], ENT_QUOTES, 'UTF-8');
    return $name=mb_convert_encoding($name, 'UTF-16LE', 'UTF-8');
}

function gnx2FetchContents($url) {

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url); //set url
    curl_setopt($ch, CURLOPT_HEADER, true); //get header
    curl_setopt($ch, CURLOPT_NOBODY, true); //do not include response body
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //do not show in browser the response
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); //follow any redirects
    curl_setopt($ch, CURLOPT_COOKIEJAR, COOKIES);
    curl_setopt($ch, CURLOPT_COOKIEFILE, COOKIES);
    curl_exec($ch);
    $new_url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL); //extract the url from the header response
    curl_close($ch);
    $ch=curl_init();
    curl_setopt($ch, CURLOPT_URL, $new_url);
    curl_setopt($ch, CURLOPT_REFERER,$new_url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        'Accept-Language:en-US,en;q=0.8',
        'Cache-Control:max-age=0',
        'Connection:keep-alive',
        'Host:'.parse_url($url)['host'],
        'Referer:'.$new_url,
        'Upgrade-Insecure-Requests:1',
        'User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36',
    ));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_COOKIEJAR, COOKIES);
    curl_setopt($ch, CURLOPT_COOKIEFILE, COOKIES);
    curl_setopt($ch, CURLOPT_ENCODING , "");
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}

function gnx2NewDataCsv($csvfile) {
    if (file_exists($csvfile))
        @unlink($csvfile);
    if (!file_exists($csvfile)) {
        $list = array();
        $fp = fopen($csvfile, 'w+');
        // $list[] = array('BASIC','','','',mb_convert_encoding('FICHA TECNICA', 'UTF-16LE', 'UTF-8'),'','','','','','','','','','','DESCARGAS');
        $list[] = array(
            'url',
            'ref',
            'name',
            'breadcrumb',
            'ilm_conexion',
            'IP','tension',
            'material',
            'familia',
            'IK',
            'clase energetica',
            'clase',
            'bomb_incl',
            'acabado',
            'bombilla',
            'imagen_ppal',
            'imagen_2d',
            'ficha tecnica',
            'fiche technique'
        );
        foreach ($list as $fields) {
            fputcsv($fp,$fields);
        }
        fclose($fp);
    }
}








/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/* FERMAX  
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
function fmxNewDataCSV($csvfile) {
    
    if (file_exists($csvfile))
        @unlink($csvfile);
    
    if (!file_exists($csvfile)) {
        $list = array();
        $fp = fopen($csvfile, 'w+');
        $list[] = array(
            'ref',                     //REF
            'name',                    //NAME
            'descripcion',             //DESCRIPTION
            'breadcrumb',              //BreadCrumb
            'url',                     //URL
            'images'                   //Images
        );
        foreach ($list as $fields) {
            fputcsv($fp, $fields);
        }
        fclose($fp);
    }
}

function fmxStartCrawling($debug = false) {

    $cli = new Client();

    // Get CATEGORIES
    $cats = fmxGetMainCategories($cli, FERMAX_URL, $debug);
    
    
    // Traverse CATEGORIES
    $i=0;
    foreach ($cats as $cat) {

        $i++;
        // Debug -> check only one category ("PLACAS" in this case) for debugging
        if ($debug && strpos(trim(strtolower($cat['name'])), "accesos") === false) continue;
        
        // Skip "Novedades" category
        if (strpos(trim(strtolower($cat['name'])), "novedades") !== false) continue;

        echo PHP_EOL.'-- '.strtoupper($cat['name']).' --------------------------- '.PHP_EOL.PHP_EOL;
        print_r($cat);

        // Get SUBCATEGORIES
        $subcats = fmxGetSubategories($cli, $cat['url'], $debug);
        
        $i=0;
        foreach ($subcats as $subcat) {

            $i++;
            if ($debug && $i>1) break; 

            $prodlst_dts = fmxScrapeSubcatAndGetTabProductsList($cli, $subcat['url'], "datasheets", $debug);
            $prodlst_cmb = fmxScrapeSubcatAndGetTabProductsList($cli, $subcat['url'], "combinations", $debug);
            $prodlst_kts = fmxScrapeSubcatAndGetTabProductsList($cli, $subcat['url'], "kits", $debug);
            $prodlst_spr = fmxScrapeSubcatAndGetTabProductsList($cli, $subcat['url'], "spare", $debug);

        }
        
    }

}

function fmxGetMainCategories($cli, $url, $debug = false) {
    
    // To to start URL
    $crawler = $cli->request('GET', $url);
    
    // Grab CATEGORIES
    $cats = $crawler->filter('ul.navbar-right a')->each(function ($cat) {
        $cat = array(
            "name" => $cat->text(),
            "url" => $cat->link()->getUri());
        return $cat;
    });
//    print_r($cats);
    return $cats;
}

function fmxGetSubategories($cli, $url, $debug = false) {
    
    $crawler = $cli->request('GET', $url);
    $subcats = $crawler->filter('div.container div.banner')->each(function ($node) {
        
        $name = $node->filter('h2 a')->first()->text();
        $description = (count($node->filter('p')))?$node->filter('p')->first()->text():"";
        $url = $node->filter('h2 a')->first()->link()->getUri();
        try {
            $subcat = array(
                "name" => $name,
                "description" => $description,
                "url" => $url
            );
        } catch (Exception $e) {
        }
        return $subcat;
        
    });
    print_r($subcats);
    return $subcats;

}

function fmxScrapeSubcatAndGetTabProductsList($cli, $url, $tab, $debug = false) {

    $crawler = $cli->request('GET', $url.'#'.$tab);
    
    $tabfichas = $crawler->filterXPath("//div[contains(@id,'".$tab."')]");
    
    //$productslist = $crawler->filter("div#nav_datasheets.tab-pane div.banner.with-button")->each(function ($node) {
    $productslist = $tabfichas->filter("div.banner.with-button")->each(function ($node) {

//        echo(count($node->filter('p'))).PHP_EOL;
//        echo($node->filter('p strong')->first()->text()).PHP_EOL;
//        return;

        if (count($node->filter('p'))) {
            $name = $node->filter('p')->eq(0)->text();
            $ref = $node->filter('p')->eq(1)->text();
            $description = $node->filter('p')->eq(2)->text();
        }
        $url = $node->filter('a.btn-primary')->first()->link()->getUri();

        try {
            $product = array(
                "name" => $name,
                "ref" => $ref,
                "description" => $description,
                "url" => $url
            );
        } catch (Exception $e) {
        }
        return $product;
        
    });
    print_r($productslist);
    return $productslist;

}

/*function nodeToArray($dom, $node) {
    if(!is_a( $dom, 'DOMDocument' ) || !is_a( $node, 'DOMNode' )) {
        return false;
    }
    $array = false; 
    if( empty( trim( $node->localName ))) { // Discard empty nodes
        return false;
    }
    if( XML_TEXT_NODE == $node->nodeType ) {
        return $node->nodeValue;
    }
    foreach ($node->attributes as $attr) { 
        $array['@'.$attr->localName] = $attr->nodeValue; 
    } 
    foreach ($node->childNodes as $childNode) { 
        if ( 1 == $childNode->childNodes->length && XML_TEXT_NODE == $childNode->firstChild->nodeType ) { 
            $array[$childNode->localName] = $childNode->nodeValue; 
        }  else {
            if( false !== ($a = self::nodeToArray( $dom, $childNode))) {
                $array[$childNode->localName] =     $a;
            }
        }
    }
    return $array; 
}*/






/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/* MIMAX
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

function mmxStartCrawling() {
    $ch=curl_init();
    $url = MIMAX_URL;
    $contents = curlDownload($ch,$url);
    
    $links = mmxGetMenuLinks($contents);
    foreach($links as $link) {
        if(stristr($link,'?'))
            $link = $link.'&limit=36';
        else
            $link = $link.'?limit=36';
        mmxParseCategory($ch,$link);
        exit();
    }
//    mmxScrapeUrlData($ch,$url='http://www.searchlightelectric.com/ceiling-lights/1019-12ab-flemish-solid-antique-brass-12-light-chandelier-with-metal-candle-covers.html');
//    mmxScrapeUrlData($ch,$url='http://www.searchlightelectric.com/ceiling-lights/1045-5-american-diner-satin-silver-5-light-fitting-with-acid-ribbed-glass.html');
//    mmxScrapeUrlData($ch,$url='http://www.searchlightelectric.com/ceiling-lights/4139-9cc-chrome-9-light-flush-with-glass-drops-buttons.html');

    curl_close($ch);
}

function mmxParseCategory($ch,$url) {

    $contents = curlDownload($ch,$url);
    $pattern = '/<div\s*class=[\"\']category\-products[\"\'].*>(.*)<div\s*class=[\"\']footer\-wrapper[\"\'].*>/Usi';
    preg_match($pattern, $contents, $result);

    $content = $result[1];

    $pattern = '/<li\s*class=[\"\']item.*>\s*<a\s*href=[\"\'](.*)[\"\'].*>/Usi';
    preg_match_all($pattern, $content, $result);
    foreach($result[1] as $link) {
        mmxScrapeUrlData($ch,$link);
    }
    $pattern = '/<a\s*class=[\"\']next.*href=[\"\'](.*)[\"\']/Usi';
    preg_match($pattern, $contents, $result);
    @$url=str_ireplace('&amp;','&',$result[1]);
    if(trim($url)!='' && stristr($url,'http'))
        mmxParseCategory($ch,$url);

}

function mmxGetMenuLinks($contents) {
    $pattern = '/<nav\s*id=[\"\']nav[\"\'].*>(.*)<\/nav>/Usi';
    preg_match($pattern, $contents, $result);

    $contents = $result[1];

    $pattern = '/<li\s*class.*>(.*)<\/li>\s*<\/ul>\s*<\/li>/Usi';
    preg_match_all($pattern, $contents, $result);
    $array=array();
    foreach($result[1] as $chunk) {
        $pattern = '/<h4>(.*)<\/ul>/Usi';
        preg_match_all($pattern, $chunk, $res);
        foreach($res[1] as $cat) {
            $pattern = '/<li>\s*<a\s*href=[\"\'](.*)[\"\']/Usi';
            preg_match_all($pattern, $cat, $re);
            foreach($re[1] as $slug) {
                if(stristr($slug,'http'))
                    $array[]=$slug;
                else
                    $array[]='http://www.searchlightelectric.com'.$slug;
            }
        }
    }
    return $array;
}

function mmxNewDataCSV($csvfile) {
    
    if (file_exists($csvfile))
        @unlink($csvfile);
    
    if (!file_exists($csvfile)) {
        $list = array();
        $fp = fopen($csvfile, 'w+');
        //$list[] = array('BASIC','','','','','','DIMENSIONS','','','','','KEY FEATURES','','','','','','','','','','SHADE','','');
        $list[] = array(
            'ref',                     //REF
            'sku',                     //SKU
            'name',                    //NAME
            'descripcion',             //DESCRIPTION
            'tarifa1',                 //PRICE1
            'tarifa2',                 //PRICE2
            'breadcrumb',              //BreadCrumb
            'url',                     //URL
            'alto',                    //Total Height (mm)
            'alto_total',              //Total Height with Chain
            'ancho_diametro',          //Width/Diameter (mm)
            'largo',                   //Length (mm)
            'alto_total2',             //Projection (mm)         
            'aislamiento',             //Class (1,2 or 3)
            'ilm_conexion',            //Lampholder
            'ilm_factor_forma',        //Lamp Type
            'potencia',                //Wattage
            'interruptor',             //Switch/Dimmer
            'tension',                 //Operating Voltage
            'ilm_bomb_incluida',       //Lamp Included
            'ilm_luminosidad',         //Lumens
            'ilm_temp_color',          //Colour Temperature
            'peso',                    //Weight
            'difusor',                 //Shade Type
            'mat_difusor',             //Shade Finish
            'medidas_difusor',         //Shade Dimensions (mm)
            'image_ppal',              //Main Product Image
            'image_sec1',              //1st Secondary Image
            'image_sec2',              //2nd Secondary Image
            'image_sec3',              //3rd Secondary Image
            'image_sec4',              //4th Secondary Image
            'image_sec5'               //5th Secondary Image
        );
        foreach ($list as $fields) {
            fputcsv($fp, $fields);
        }
        fclose($fp);
    }
}

// Scrapes all data given a product's URL
function mmxScrapeUrlData($ch, $url) {
    
    $contents = curlDownload($ch, $url);

    $name =                 mmxName($contents);
    $ref =                  mmxSku($contents);
    $sku =                  MIMAX_IMG_PRFX.mb_strtolower($ref);
    $desc =                 mmxDesc($contents);
    $price =                mmxPrice($contents);
    preg_match('/[^\d.]*(\d+\.\d+)/', $price, $match);                  //*** Extract decimal number
    $price1 =               str_replace('.',',',$match[1]);             //echo $price.PHP_EOL.$price1.PHP_EOL;
    $price2 =               str_replace('.',',',round($price1/EXCH_RATE_GPB_EUR, 2));
    $breadcumb =            mmxBreadcumb($contents);
    $totalHeight =          mmxTotalHeight($contents);
    $totalHeightWithChain = mmxTotalHeightWithChain($contents);
    $widthDiam =            mmxWidthDiam($contents);
    $length =               mmxLength($contents);
    $projection =           mmxProjection($contents);
    $class =                mmxClass($contents);
    $lampholder =           mmxLampholder($contents);
    $lampType =             mmxLampType($contents);
    $wattage =              mmxWattage($contents);
    $switch =               mmxSwitchn($contents);
    $opvoltage =            mmxOpvoltage($contents);
    $lampincluded =         mmxLampincluded($contents);
    $lumens =               mmxLumens($contents);
    $colortemp =            mmxColortemp($contents);
    $weight =               mmxWeight($contents);
    $shadetype =            mmxShadetype($contents);
    $shadefinish =          mmxShadefinish($contents);
    $shadedimensions =      mmxShadedimensions($contents);
    $image =                mmxGetImages($contents, UNIX_RESIZE_IMAGES);
    $array = array(
        'ref' =>                  $ref,
        'sku' =>                  $sku,
        'name' =>                 $name,
        'desc' =>                 $desc,
        'price1' =>               $price1,
        'price2' =>               $price2,
        'breadcumb' =>            $breadcumb,
        'url' =>                  $url,
        'totalHeight' =>          $totalHeight,
        'totalHeightWithChain' => $totalHeightWithChain,
        'widthDiam' =>            $widthDiam,
        'length' =>               $length,
        'projection' =>           $projection,
        'class' =>                $class,
        'lampholder' =>           $lampholder,
        'lampType' =>             $lampType,
        'wattage' =>              $wattage,
        'switch' =>               $switch,
        'opvoltage' =>            $opvoltage,
        'lampincluded' =>         $lampincluded,
        'lumens' =>               $lumens,
        'colortemp' =>            $colortemp,
        'weight' =>               $weight,
        'shadetype' =>            $shadetype,
        'shadefinish' =>          $shadefinish,
        'shadedimensions' =>      $shadedimensions,
        'image' =>$image
    );
    echo PHP_EOL."PRODUCTO:".PHP_EOL."----------".PHP_EOL;   print_r($array);
    saveArrayInCsv(MIMAX_CSV,$array);
}

function mmxGetImages($contents, $resize = false) {

    $log = new Logging();
    
    $contents = str_replace("%<div></div>%", "", $contents);               //deletes shitty "<div></div>"

    $array = array();
    
    //MAIN IMAGE
    $pattern = '/<img\s*id=[\"\']image\-main[\"\'].*src=[\"\'](.*)[\"\']/Usi';
    preg_match($pattern, $contents, $result);                    //echo PHP_EOL."IMAGENE PRINCIPAL:".PHP_EOL."----------".PHP_EOL;   print_r($result[1]);
    $imgurl =  $result[1];                                       //echo PHP_EOL."$imgurl: ".$imgurl;
    $imgname = explode('/',$imgurl);                             //echo PHP_EOL."$imgname: ".$imgname;
    $imgname = MIMAX_IMG_PRFX.mb_strtolower(end($imgname));      //echo PHP_EOL."$imgname: ".$imgname.PHP_EOL;
    if(!is_dir(MIMAX_LOC_IMG_ORG1))
        mkdir(MIMAX_LOC_IMG_ORG1,0777);
    downloadFile($imgurl, MIMAX_LOC_IMG_ORG1.$imgname);
    $array[]=$imgname;
    if ($resize)
        if (!resizeOptimizeImage(
              MIMAX_LOC_IMG_ORG1.$imgname,
              MIMAX_LOC_IMG_ORG2.$imgname,
              RESIZED_IMAGE_MAX_WIDTH, RESIZED_IMAGE_MAX_HEIGHT))
            $log->lecho ("Error al procesar la imagen ".$imgname);
                   

    //GALLERY IMAGES
    //$pattern = '/<img\s*src=[\"\'](.*)[\"\']/Usi';
    $pattern = '/\<img\s*id\=[\"\']image\-\d[\"\']\s*class\=[\"\']gallery\-image[\"\']\s*src\=[\"\'](.*)[\"\']/Usi';
    preg_match_all($pattern, $contents, $result);       //echo PHP_EOL."IMAGENES GALER?A:".PHP_EOL."----------".PHP_EOL;   print_r($result[1]);
    $first = true;
    foreach($result[1] as $imgurl) {
        if ($first) { $first=false; continue; }
        $imgname = explode('/',$imgurl);
        $imgname = MIMAX_IMG_PRFX.str_replace("__","_",mb_strtolower(end($imgname)));
        if (!is_dir(MIMAX_LOC_IMG_ORG1)) mkdir(MIMAX_LOC_IMG_ORG1,0777);
        downloadFile($imgurl, MIMAX_LOC_IMG_ORG1.$imgname);
        $array[]=$imgname;
        if ($resize)
            if (!resizeOptimizeImage(
                  MIMAX_LOC_IMG_ORG1.$imgname,
                  MIMAX_LOC_IMG_ORG2.$imgname,
                  RESIZED_IMAGE_MAX_WIDTH, RESIZED_IMAGE_MAX_HEIGHT))
                $log->lecho ("Error al procesar la imagen ".$imgname);
    }
    
    //$array=array_unique($array);

    return implode(",/",$array);

}

function mmxShadetype($contents) {
    $pattern = '/<td\s*class=[\"\']spec\-label[\"\']>\s*Shade\s*Type\s*<\/td>\s*<td.*>(.*)<\/td>/Usi';
    preg_match($pattern, $contents, $match);
    $result = trim(preg_replace('/\s\s+/Usi','',preg_replace('/<.*>/Usi','',$match[1])));
    return $result;
}
function mmxShadefinish($contents) {
    $pattern = '/<td\s*class=[\"\']spec\-label[\"\']>\s*Shade\s*Finish\s*<\/td>\s*<td.*>(.*)<\/td>/Usi';
    preg_match($pattern, $contents, $match);
    $result = trim(preg_replace('/<.*>/Usi','',$match[1]));
    return $result;
}
function mmxShadedimensions($contents) {
    $pattern = '/<td\s*class=[\"\']spec\-label[\"\']>\s*Shade\s*Dimensions\s*\(mm\)\s*<\/td>\s*<td.*>(.*)<\/td>/Usi';
    preg_match($pattern, $contents, $match);
    $result = trim(preg_replace('/\s\s+/Usi','',preg_replace('/<.*>/Usi','',$match[1])));
    return $result;
}
function mmxTotalHeight($contents) {
    $pattern = '/<td\s*class=[\"\']spec\-label[\"\']>\s*Total\s*Height\s*\(mm\)\s*<\/td>\s*<td.*>(.*)<\/td>/Usi';
    preg_match($pattern, $contents, $match);
    $result = trim(preg_replace('/\s\s+/Usi','',preg_replace('/<.*>/Usi','',$match[1])));
    if(is_numeric($result))   return $result/10;
    elseif($result=='0')      return "";
    else                      return $result;
}
function mmxTotalHeightWithChain($contents) {
    $pattern = '/<td\s*class=[\"\']spec\-label[\"\']>\s*Total\s*Height\s*with\s*Chain\s*<\/td>\s*<td.*>(.*)<\/td>/Usi';
    preg_match($pattern, $contents, $match);
    $result = trim(preg_replace('/<.*>/Usi','',$match[1]));
    $result = str_ireplace("mm", "", $result);
    if(is_numeric($result))   return $result/10;
    elseif($result=='0')      return "";
    else                      return $result;
}
function mmxWidthDiam($contents) {
    $pattern = '/<td\s*class=[\"\']spec\-label[\"\']>\s*Width\/Diameter\s*\(mm\)\s*<\/td>\s*<td.*>(.*)<\/td>/Usi';
    preg_match($pattern, $contents, $match);
    $result = trim(preg_replace('/<.*>/Usi','',$match[1]));
    if(is_numeric($result))   return $result/10;
    elseif($result=='0')      return "";
    else                      return $result;
}
function mmxLength($contents) {
    $pattern = '/<td\s*class=[\"\']spec\-label[\"\']>\s*Length\s*\(mm\)\s*<\/td>\s*<td.*>(.*)<\/td>/Usi';
    preg_match($pattern, $contents, $match);
    $result = trim($match[1]);
    if(is_numeric($result))   return $result/10;
    elseif($result=='0')      return "";
    else                      return $result;
}
function mmxProjection($contents) {
    $pattern = '/<td\s*class=[\"\']spec\-label[\"\']>\s*Projection\s*\(mm\)\s*<\/td>\s*<td.*>(.*)<\/td>/Usi';
    preg_match($pattern, $contents, $match);
    $result = trim($match[1]);
    $result = str_ireplace("mm", "", $result);
    if(is_numeric($result))   return $result/10;
    elseif($result=='0')      return "";
    else                      return $result;
}
function mmxClass($contents) {
    $pattern = '/<td\s*class=[\"\']spec\-label[\"\']>\s*Class\s*\(1\,2\s*or\s*3\)\s*<\/td>\s*<td.*>(.*)<\/td>/Usi';
    preg_match($pattern, $contents, $match);
    $result = trim(preg_replace('/\s\s+/Usi','',preg_replace('/<.*>/Usi','',$match[1])));
    if($result[0]=="1")       return "Clase I (toma tierra)";
    elseif($result[0]=="2")   return "Clase II (doble aislam.)";
    elseif($result[0]=="3")   return "Clase III (bajo voltaje)";
    else return $result;
}
function mmxLampholder($contents) {
    $pattern = '/<td\s*class=[\"\']spec\-label[\"\']>\s*Lampholder\s*<\/td>\s*<td.*>(.*)<\/td>/Usi';
    preg_match($pattern, $contents, $match);
    $result = trim(preg_replace('/<.*>/Usi','',$match[1]));
    if(strpos($result,"G9")!==false)            return "G9";
    elseif(strpos($result,"G4")!==false)        return "G4";
    elseif(strpos($result,"E14")!==false)       return "E14";
    elseif(strpos($result,"E27")!==false)       return "E27";
    else return $result;
}
function mmxLampType($contents) {
    $pattern = '/<td\s*class=[\"\']spec\-label[\"\']>\s*Lamp\s*Type\s*<\/td>\s*<td.*>(.*)<\/td>/Usi';
    preg_match($pattern, $contents, $match);
    $result = trim(preg_replace('/<.*>/Usi','',$match[1]));
    if(strpos(strtolower($result),"halogen")!==false)       return "Halógena";
    elseif(strpos(strtolower($result),"dichroic")!==false)  return "Halógena";
    elseif(strpos(strtolower($result),"capsule")!==false)   return "Halógena";
    elseif(strpos(strtolower($result),"golf")!==false)      return "Según bombilla";
    elseif(strpos(strtolower($result),"led")!==false)       return "LED";
    elseif(strpos(strtolower($result),"candle")!==false)    return "Según bombilla";
    else return $result;
}
function mmxWattage($contents) {
    $pattern = '/<td\s*class=[\"\']spec\-label[\"\']>\s*Wattage\s*<\/td>\s*<td.*>(.*)<\/td>/Usi';
    preg_match($pattern, $contents, $match);
    $result = trim($match[1]);
    if($result==0)            return "";
    else                      return $result."W";
}
function mmxSwitchn($contents) {
    $pattern = '/<td\s*class=[\"\']spec\-label[\"\']>\s*Switch\/Dimmer\s*<\/td>\s*<td.*>(.*)<\/td>/Usi';
    preg_match($pattern, $contents, $match);
    $result = trim($match[1]);
    return $result;
}
function mmxOpvoltage($contents) {
    $pattern = '/<td\s*class=[\"\']spec\-label[\"\']>\s*Operating\s*Voltage\s*<\/td>\s*<td.*>(.*)<\/td>/Usi';
    preg_match($pattern, $contents, $match);
    $result = trim(preg_replace('/\s\s+/Usi','',preg_replace('/<.*>/Usi','',$match[1])));
    if (strStartsWith($result, "220")||strStartsWith($result, "230")||strStartsWith($result, "240")) return "220-240V 50/60Hz";
    else                      return $result;
}
function mmxLampincluded($contents) {
    $pattern = '/<td\s*class=[\"\']spec\-label[\"\']>\s*Lamp\s*Included\s*<\/td>\s*<td.*>(.*)<\/td>/Usi';
    preg_match($pattern, $contents, $match);
    $result = trim(preg_replace('/<.*>/Usi','',$match[1]));
    if ($result=="Yes")       return "Sí";
    else return $result;
}
function mmxLumens($contents) {
    $pattern = '/<td\s*class=[\"\']spec\-label[\"\']>\s*Lumens\s*<\/td>\s*<td.*>(.*)<\/td>/Usi';
    preg_match($pattern, $contents, $match);
    $result = trim(preg_replace('/<.*>/Usi','',$match[1]));
    if($result==0)            return "";
    else                      return $result;
}
function mmxColortemp($contents) {
    $pattern = '/<td\s*class=[\"\']spec\-label[\"\']>\s*Colour\s*Temperature\s*<\/td>\s*<td.*>(.*)<\/td>/Usi';
    preg_match($pattern, $contents, $match);
    $result = trim($match[1]);
    return $result;
}
function mmxWeight($contents) {
    $pattern = '/<td\s*class=[\"\']spec\-label[\"\']>\s*Weight\s*<\/td>\s*<td.*>(.*)<\/td>/Usi';
    preg_match($pattern, $contents, $match);
    $result = trim(preg_replace('/[\s\p{L}]/Usi','',$match[1]));
    if($result==0)            return "";
    else                      return $result;
}
function mmxBreadcumb($contents) {
    $pattern = '/<div\s*class=[\"\']breadcrumbs[\"\'].*>(.*)<\/div>/Usi';
    preg_match($pattern, $contents, $match);
    $result = trim(preg_replace('/\s\s+/Usi','',preg_replace('/<.*>/Usi','',$match[1])));
    return $result;
}
function mmxPrice($contents) {
    $pattern = '/<div\s*class=[\"\']short\-description[\"\'].*>.*<span\s*class=[\"\']price[\"\'].*>(.*)<\/span>/Usi';
    preg_match($pattern, $contents, $match);
    $result = trim(preg_replace('/<.*>/Usi','',$match[1]));
    return $result;
}
function mmxDesc($contents) {
    $pattern = '/<div\s*class=[\"\']short\-description[\"\'].*>(.*)<\/div>/Usi';
    preg_match($pattern, $contents, $match);
    $result = trim(preg_replace('/<.*>/Usi','',$match[1]));
    return $result;
}
function mmxSku($contents) {
    $pattern = '/<div\s*class=[\"\']product\-sku[\"\'].*>\s*<span.*>(.*)</Usi';
    preg_match($pattern, $contents, $match);
    $result = trim($match[1]);
    return $result;
}
function mmxName($contents) {
    $pattern = '/<title>(.*)<\/title>/Usi';
    preg_match($pattern, $contents, $match);
    $result = trim($match[1]);
    return $result;
}












/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/* UNIX  
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

function unixCreateXML($file) {
    if (file_exists($file))
        unlink($file);
    if (!file_exists($file)) {
        $content ='<?xml version="1.0" encoding="utf-8"?>
<DATA>
</DATA>';
        file_put_contents($file,$content);
    }
}
function unixInsertinXML($file,$data) {
    if (file_exists($file)) {
        $contents=file_get_contents($file);
        $contents=str_ireplace('</DATA>',$data.PHP_EOL.'</DATA>',$contents);
        file_put_contents($file,$contents);
    }
    else
        die("$file XML File Not Found");
}
function unixMoreimages($content, $resize = false) {
    $log = new Logging();
    $pattern = '/class=[\"\']more\-views[\"\'].*>(.*)<\/li>\s*<\/ul>\s*<\/div>/Usi';
    preg_match($pattern, $content, $result);
    $cont=$result[1];
    $pattern = '/<img.*src=[\"\'](.*)[\"\']/Usi';
    preg_match_all($pattern, $cont, $result);
    $i=0;
    $moreimgs='';
    foreach($result[1] as $image) {
        if (stristr($image,'thumbnail')) {
            if ( $i>0) {
                $image=preg_replace('/\/thumbnail\/.*\//Usi','/image/',$image);
                if (UNIX_DOWNLOAD_IMAGES) {
                    $imgname=explode('/',$image);
                    $imgname=end($imgname);
                    downloadFile($image, UNIX_LOC_IMG_ORG1.UNIX_IMG_PRFX.$imgname);
                    if ($resize)
                        if (!resizeOptimizeImage(
                              UNIX_LOC_IMG_ORG1.UNIX_IMG_PRFX.$imgname,
                              UNIX_LOC_IMG_ORG2.UNIX_IMG_PRFX.$imgname,
                              RESIZED_IMAGE_MAX_WIDTH, RESIZED_IMAGE_MAX_HEIGHT))
                            $log->lecho ("Error al procesar la imagen ".$imgname);
               }
               $moreimgs.='<ADDITIONAL_IMAGE_LINK'.sprintf('%02d', $i).'>'.$image.'</ADDITIONAL_IMAGE_LINK'.sprintf('%02d', $i).'>'.PHP_EOL;
            }
            $i++;
        }
    }
    return $moreimgs;
}
function unixMinqty($content) {
    $pattern = '/id=[\"\']qty[\"\'].*value=[\"\'](.*)[\"\']/Usi';
    preg_match($pattern, $content, $result);
    if (trim($result[1])=='') {
        $pattern = '//Usi';
        preg_match($pattern, $content, $result);
    }
    return(int)trim(preg_replace('/\s\s+/Usi', '',preg_replace('/<.*>/Usi', '', $result[1])));
}
function unixLengthwidth($content) {
    $pattern = '/Medidas\s*<\/span>\s*<span.*>(.*)<\/span>/Usi';
    preg_match($pattern, $content, $result);
    if (trim($result[1])=='') {
        $pattern = '//Usi';
        preg_match($pattern, $content, $result);
    }
    $cont=trim($result[1]);    //200.00 cm. x 140.00 cm
    $array=array();
    $arr=explode(' ',$cont);
    $unit='';
    $arr=array_filter($arr);
    foreach($arr as $item) {
        if(is_numeric($item))
            $array[]=$item;
        if (trim($item)!='' && trim($item)!='.' && strtolower(trim($item))!='x' )
            $unit=trim($item);
    }
    $unit=preg_replace('/[^a-zA-Z]+/Usi','',$unit);
    return array('length'=>$array[0],'width'=>$array[0],'height'=>$array[0],'unit'=>$unit);
}
function unixColor($content) {
    $pattern = '/color\s*<\/span>\s*<span.*>(.*)<\/span>/Usi';
    preg_match($pattern, $content, $result);
    if (trim($result[1])=='') {
        $pattern = '//Usi';
        preg_match($pattern, $content, $result);
    }
    return @trim(preg_replace('/\s\s+/Usi', '',preg_replace('/<.*>/Usi', '', $result[1])));
}
function unixAssortment($content) {
    $pattern = '/surtido\s*<\/span>\s*<span.*>(.*)<\/span>/Usi';
    preg_match($pattern, $content, $result);
    if (trim($result[1])=='') {
        $pattern = '//Usi';
        preg_match($pattern, $content, $result);
    }
    return @trim(preg_replace('/\s\s+/Usi', '',preg_replace('/<.*>/Usi', '', $result[1])));
}
function unixUse($content) {
    $pattern = '/uso\s*<\/span>\s*<span.*>(.*)<\/span>/Usi';
    preg_match($pattern, $content, $result);
    if (trim($result[1])=='') {
        $pattern = '//Usi';
        preg_match($pattern, $content, $result);
    }
    return @trim(preg_replace('/\s\s+/Usi', '',preg_replace('/<.*>/Usi', '', $result[1])));
}
function unixCollection($content) {
    $pattern = '/coleccion\s*<\/span>\s*<span.*>(.*)<\/span>/Usi';
    preg_match($pattern, $content, $result);
    if (trim($result[1])=='') {
        $pattern = '//Usi';
        preg_match($pattern, $content, $result);
    }
    return @trim(preg_replace('/\s\s+/Usi', '',preg_replace('/<.*>/Usi', '', $result[1])));
}
function unixPattern($content) {
    $pattern = '/estilo\s*<\/span>\s*<span.*>(.*)<\/span>/Usi';
    preg_match($pattern, $content, $result);
    if (trim($result[1])=='') {
        $pattern = '//Usi';
        preg_match($pattern, $content, $result);
    }
    return @trim(preg_replace('/\s\s+/Usi', '',preg_replace('/<.*>/Usi', '', $result[1])));
}
function unixMaterial($content) {
    $pattern = '/material\s*<\/span>\s*<span.*>(.*)<\/span>/Usi';
    preg_match($pattern, $content, $result);
    if (trim($result[1])=='') {
        $pattern = '//Usi';
        preg_match($pattern, $content, $result);
    }
    return @trim(preg_replace('/\s\s+/Usi', '',preg_replace('/<.*>/Usi', '', $result[1])));
}
function unixPiece($content) {
    $pattern = '/pieza\s*<\/span>\s*<span.*>(.*)<\/span>/Usi';
    preg_match($pattern, $content, $result);
    if (trim($result[1])=='') {
        $pattern = '//Usi';
        preg_match($pattern, $content, $result);
    }
    return @trim(preg_replace('/\s\s+/Usi', '',preg_replace('/<.*>/Usi', '', $result[1])));
}
function unixSeason($content) {
    $pattern = '/temporada\s*<\/span>\s*<span.*>(.*)<\/span>/Usi';
    preg_match($pattern, $content, $result);
    if (trim($result[1])=='') {
        $pattern = '//Usi';
        preg_match($pattern, $content, $result);
    }
    return @trim(preg_replace('/\s\s+/Usi', '',preg_replace('/<.*>/Usi', '', $result[1])));
}
function unixModel($content) {
    $pattern = '/modelo\s*<\/span>\s*<span.*>(.*)<\/span>/Usi';
    preg_match($pattern, $content, $result);
    if (trim($result[1])=='') {
        $pattern = '//Usi';
        preg_match($pattern, $content, $result);
    }
    return @trim(preg_replace('/\s\s+/Usi', '',preg_replace('/<.*>/Usi', '', $result[1])));
}
function unixFetchdisp($content) {
    $pattern = '/Fecha\s*disponibilidad\s*<\/dt>\s*<dd.*>(.*)</Usi';
    preg_match($pattern, $content, $result);
    if (trim($result[1])=='') {
        $pattern = '//Usi';
        preg_match($pattern, $content, $result);
    }
    return @trim(preg_replace('/\s\s+/Usi', '',preg_replace('/<.*>/Usi', '', $result[1])));
}
function unixMultipack($content) {
    $pattern = '/set\s*<\/span>\s*<span.*>(.*)<\/span>/Usi';
    preg_match($pattern, $content, $result);
    if (trim($result[1])=='') {
        $pattern = '//Usi';
        preg_match($pattern, $content, $result);
    }
    return @trim(preg_replace('/\s\s+/Usi', '',preg_replace('/<.*>/Usi', '', $result[1])));
}
function unixAvailableunits1($content) {    // Ixia
    $pattern = '/>\s*Disponible\s*hoy\s*<\/dt>\s*<dd\s*>(.*)<\/dd>/Usi';
    return unixAvailableunits($content,$pattern);
}
function unixAvailableunits2($content) {    // Unimasa - Juinsa
    $pattern = '/>\s*Uds\.\s*disponibles\s*<\/dt>\s*<dd\s*class=[\"\']positivo[\"\'].*>(.*)<\/dd>/Usi';
    return unixAvailableunits($content,$pattern);
}
function unixAvailableunits($content,$pattern) {
    preg_match($pattern, $content, $result);
    if (trim($result[1])=='') {
        $pattern = '//Usi';
        preg_match($pattern, $content, $result);
    }
    return @trim(html_entity_decode(preg_replace('/\s\s+/Usi', '',preg_replace('/<.*>/Usi', '', $result[1]))));
}
function unixRef($content) {
    $pattern = '/<p\s*id=[\"\']ref[\"\'].*>(.*)</Usi';
    preg_match($pattern, $content, $result);
    return @trim(preg_replace('/\s\s+/Usi', '',preg_replace('/<.*>/Usi', '', $result[1])));
}
function unixPrice($content) {
    $pattern = '/<div\s*class=[\"\']prize[\"\'].*>.*class=[\"\']price[\"\'].*>(.*)</Usi';
    preg_match($pattern, $content, $result);
    $price=@trim(preg_replace('/\s\s+/Usi', '', preg_replace('/[^0-9\,\.]+/Usi', '', $result[1])));
    return $price=str_ireplace(',','.',$price);
}
function unixDescription($content) {
    $pattern = '/class=[\"\']std[\"\'].*>(.*)<\/div>/Usi';
    preg_match($pattern, $content, $result);
    $desc=@trim(html_entity_decode(preg_replace('/\s\s+/Usi', '',preg_replace('/<.*>/Usi', '',preg_replace('/<script.*<\/script>/Usi', '', $result[1])))));
    //return $desc=html_entity_decode($desc, ENT_QUOTES, 'UTF-8');
    return $desc=$desc;
}
function unixName($content) {
    $pattern = '/<h1>(.*)<\/h1>/Usi';
    preg_match($pattern, $content, $result);
    $name=@trim(html_entity_decode(preg_replace('/\s\s+/Usi', '',preg_replace('/<.*>/Usi', '', $result[1]))));
    //return $name=html_entity_decode($name, ENT_QUOTES, 'UTF-8');
    return $name=$name;
}
function unixImage($content, $resize = false) {
    $log = new Logging();
    $pattern = '/<p\s*class=[\"\']product\-image\s*product\-image\-zoom[\"\'].*>\s*<a.*href=[\"\'](.*)[\"\']/Usi';
    preg_match($pattern, $content, $result);
    $image=@trim(preg_replace('/\s\s+/Usi', '',preg_replace('/<.*>/Usi', '', $result[1])));
    if (UNIX_DOWNLOAD_IMAGES) {
        $imgname=explode('/',$image);
        $imgname=end($imgname);
        downloadFile($image, UNIX_LOC_IMG_ORG1.UNIX_IMG_PRFX.$imgname);
        if ($resize)
            if (!resizeOptimizeImage(
                  UNIX_LOC_IMG_ORG1.UNIX_IMG_PRFX.$imgname,
                  UNIX_LOC_IMG_ORG2.UNIX_IMG_PRFX.$imgname,
                  RESIZED_IMAGE_MAX_WIDTH, RESIZED_IMAGE_MAX_HEIGHT))
                $log->lecho ("Error al procesar la imagen ".$imgname);
    }
    return $image;
}
function unixBreadcumb($content) {
    $pattern = '/<div\s*class=[\"\']breadcrumbs[\"\'].*>(.*)<\/div>/Usi';
    preg_match($pattern, $content, $result);

    $bc=@trim(html_entity_decode(preg_replace('/\s\s+/Usi', '',preg_replace('/<.*>/Usi', '', $result[1]))));
    $arr=explode('/',$bc);
    return array('category'=>trim($arr[1]),'type'=>trim($arr[2]));
}

function unixProductUrl($content) {
    $pattern = '/<a.*href=[\"\'](.*)[\"\']/Usi';
    preg_match($pattern, $content, $result);
    return @trim(preg_replace('/\s\s+/Usi', '', $result[1]));
}
function unixProductPrice($content) {
    $pattern = '/<span\s*class=[\"\']price[\"\']>(.*)</Usi';
    preg_match($pattern, $content, $result);
    $price=@trim(preg_replace('/\s\s+/Usi', '', preg_replace('/[^0-9\,\.]+/Usi', '', $result[1])));
    return $price=str_ireplace(',','.',$price);
}
function unixNextPageNumber($content) {
    $pattern = '/<a\s*class=[\"\']next\s*i\-next[\"\']\s*href=[\"\'](.*)[\"\']/Usi';
    preg_match($pattern, $content, $result);
    @$nextpage=trim($result[1]);
    if (trim($nextpage)!='')
        return $nextpage;
    else
        return false;
}
function convertMonthToDate($fetchdisp) {
    $array=array(
        "01"=>'Enero',
        "02"=>'Febrero',
        "03"=>'Marzo',
        "04"=>'Abril',
        "05"=>'Mayo',
        "06"=>'Junio',
        "07"=>'Julio',
        "08"=>'Agosto',
        "09"=>'Septiembre',
        "10"=>'Octubre',
        "11"=>'Noviembre',
        "12"=>'Diciembre',
    );
    if ($fetchdisp!='.' && $fetchdisp!='' && $fetchdisp!='0000-00-00' ) {
        foreach($array as $key=>$month) {
            if (stristr($fetchdisp,$month)) {
            $year = ($key>date(m))? date("Y") : date("Y")+1;
                $fetchdisp = $year . "-" . $key . "-" . "01";
                break;
            }
        }
    }
    return $fetchdisp;
}
function pretreat($content) {
    //return $content;
    return html_entity_decode($content, ENT_QUOTES, 'UTF-8');
}









?>